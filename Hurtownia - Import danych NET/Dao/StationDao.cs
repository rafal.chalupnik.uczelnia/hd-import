﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Hurtownia___Import_danych_NET.Dto;

namespace Hurtownia___Import_danych_NET.Dao
{
    public static class StationDao
    {
        public static List<Station> GetStations()
        {
            using (var connection = new SqlConnection(Constants.ConnectionString))
            {
                return connection.Query<Station>("SELECT * FROM [dbo].[Dim_Station]").ToList();
            }
        }
    }
}