﻿using System.Collections.Generic;
using System.Data.SqlClient;
using Dapper;
using Hurtownia___Import_danych_NET.Dto;

namespace Hurtownia___Import_danych_NET.Dao
{
    public static class ContaminationMeasurementDao
    {
        public static void Add(List<ContaminationMeasurement> _measurements)
        {
            using (var connection = new SqlConnection(Constants.ConnectionString))
            {
                connection.Execute("INSERT INTO [dbo].[Fact_ContaminationMeasurement] VALUES (@Date, @Station, @Contamination, @Value)", _measurements);
            }
        }
    }
}