﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using Hurtownia___Import_danych_NET.Dao;
using Hurtownia___Import_danych_NET.Dto;
using Excel = Microsoft.Office.Interop.Excel;

namespace Hurtownia___Import_danych_NET
{
    public class ContanimentDataImport
    {
        private List<string> contaminations_;
        private readonly Excel.Application excelApplication_ = new Excel.Application();
        private List<Station> stations_;

        private void ProcessFile(string _filePath)
        {
            var workbook = excelApplication_.Workbooks.Open(_filePath);
            var worksheet = workbook.Sheets[1] as Excel.Worksheet;
            var range = worksheet.UsedRange;

            var columns = range.Columns.Count;
            var rows = range.Rows.Count;

            var contamination = range.Cells[2, 2].Value as string;
            var stations = new string[columns + 1];

            for (var column = 2; column <= columns; column++)
                stations[column] = (range.Cells[1, column].Value as string);

            var measurements = new List<ContaminationMeasurement>();

            for (var row = 4; row <= rows; row++)
            {
                var date = range.Cells[row, 1].Value.ToString();

                for (var column = 2; column <= columns; column++)
                {
                    var value = range.Cells[row, column].Value;

                    if (value != null)
                    {
                        measurements.Add(new ContaminationMeasurement()
                        {
                            Contamination = contamination,
                            Date = DateTime.Parse(date),
                            Station = stations_.First(_station => _station.OldStationCode == stations[column]).Station_PK,
                            Value = (decimal)value
                        });
                    }
                }
            }

            if (!contaminations_.Contains(contamination))
                ContaminationDao.AddContaminations(new Contamination() {Contamination_PK = contamination});
            
            ContaminationMeasurementDao.Add(measurements);

            contaminations_.Add(contamination);
        }

        public void Start(string _dataDirectory)
        {
            stations_ = StationDao.GetStations();
            contaminations_ = ContaminationDao.GetContaminations()
                .Select(_contamination => _contamination.Contamination_PK).ToList();

            var filesToProcess = Directory.GetFiles(_dataDirectory);

            for (var i = 0; i < filesToProcess.Length; i++)
            {
                Console.Clear();
                Console.WriteLine($"Processing file {i + 1}/{filesToProcess.Length}");
                ProcessFile(filesToProcess[i]);
            }

            Console.WriteLine("Done!");
        }
    }
}