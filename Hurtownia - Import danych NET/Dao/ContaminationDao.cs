﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Hurtownia___Import_danych_NET.Dto;

namespace Hurtownia___Import_danych_NET.Dao
{
    public static class ContaminationDao
    {
        public static void AddContaminations(Contamination _contamination)
        {
            using (var connection = new SqlConnection(Constants.ConnectionString))
            {
                connection.Execute("INSERT INTO [dbo].[Dim_Contamination] VALUES (@Contamination_PK)", _contamination);
            }
        }

        public static List<Contamination> GetContaminations()
        {
            using (var connection = new SqlConnection(Constants.ConnectionString))
            {
                return connection.Query<Contamination>("SELECT * FROM [dbo].[Dim_Contamination]").ToList();
            }
        }
    }
}