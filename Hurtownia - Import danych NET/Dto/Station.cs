﻿namespace Hurtownia___Import_danych_NET.Dto
{
    public class Station
    {
        public int Station_PK { get; set; }
        public string Place { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public string OldStationCode { get; set; }
    }
}