﻿using System;

namespace Hurtownia___Import_danych_NET.Dto
{
    public class ContaminationMeasurement
    {
        public DateTime Date { get; set; }
        public int Station { get; set; }
        public string Contamination { get; set; }
        public decimal Value { get; set; }
    }
}